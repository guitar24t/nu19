package frc.team125.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import frc.team125.robot.utils.CsvWriter;
import frc.team125.robot.utils.LogTimer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import org.junit.jupiter.api.Test;


public class TestCsvWriter {
  private static final String TEST_FILE_NAME = "src/test/output/test.csv";
  private BufferedReader reader;
  private String[] fields = {"field1", "field2", "field3"};
  private CsvWriter testLogger;

  public TestCsvWriter() {
    initWriter(TEST_FILE_NAME, fields);
    initBuffer();
  }

  void initWriter(String fileName, String[] fields) {
    testLogger = new CsvWriter(fileName, fields, new StepTimer(0.5));
  }

  void initBuffer() {
    try {
      reader = new BufferedReader(new FileReader(TEST_FILE_NAME));
      assertTrue(reader != null);
      assertFalse(reader.ready(), "Shouldn't be re-initializing a buffer that's already reading");
    } catch (IOException e) {
      fail("Can't create file:%s" + TEST_FILE_NAME);
    }
  }

  @Test
  public void testLogging() {
    HashMap<String, Object> updateTest = new HashMap<>();


    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(0, testLogger.getLinesToWrite().size());

    // Step 1. Simulate a subsytem's fields being updated(use updateTest.put() to log values to it
    // and then do `testLogger.update(updateTest);` to send them to the logger)
    updateTest.put("field1", 1);
    updateTest.put("field2", "yes");
    updateTest.put("field3", 'd');
    testLogger.update(updateTest);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(1, testLogger.getLinesToWrite().size());

    // Step 2. Repeat Step 1 2 more times (a total of 3 updates)
    updateTest.put("field2", true);
    updateTest.put("field1", "nutrons");
    updateTest.put("field3", 125);
    testLogger.update(updateTest);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(2, testLogger.getLinesToWrite().size());

    updateTest.put("field3", "skskskks");
    updateTest.put("field1", 5);
    updateTest.put("field2", 'c');
    testLogger.update(updateTest);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(3, testLogger.getLinesToWrite().size());
    testLogger.write();
    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(0, testLogger.getLinesToWrite().size());

    assertEquals(1.5, testLogger.getCurrentFields().get("timestamp"));
    assertEquals(5, testLogger.getCurrentFields().get("field1"));
    assertEquals('c', testLogger.getCurrentFields().get("field2"));
    assertEquals("skskskks", testLogger.getCurrentFields().get("field3"));

  }

  @Test
  public void testCsvWriting() {
    HashMap<String, Object> updateTest = new HashMap<>();

    // Repeat Steps 1, 2, and 5 from testLogging();
    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(0, testLogger.getLinesToWrite().size());

    updateTest.put("field1", 1);
    updateTest.put("field2", "yes");
    updateTest.put("field3", 'd');
    testLogger.update(updateTest);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(1, testLogger.getLinesToWrite().size());

    updateTest.put("field2", true);
    updateTest.put("field1", "nutrons");
    updateTest.put("field3", 125);
    testLogger.update(updateTest);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(2, testLogger.getLinesToWrite().size());

    updateTest.put("field3", "skskskks");
    updateTest.put("field1", 5);
    updateTest.put("field2", 'c');
    testLogger.update(updateTest);

    testLogger.write();

    initBuffer(); // use reader.readLine() to read from the csv writer that is being written to

    try {
      // Step 1: assert that reader.readLine() returns null (because you haven't flushed to the file
      // yet

      assertEquals(null, reader.readLine());
      // Step 2: run 'testLogger.flushToFile()'

      testLogger.flushToFile();
      // Step 3: write 4 assertEquals. the "actual" field of the methods should be 'reader
      // .readLine(), and the "expected" field should be what that line in the csv file should
      // look like. This works because each time 'reader.readLine()' is run, it automatically goes
      // to the next. The last time you run it, it should return 'null' because at that point you
      // will have gone through every line.
      //

      assertEquals("timestamp, field1, field2, field3", reader.readLine());
      assertEquals("0.5, 1, yes, d", reader.readLine());
      assertEquals("1.0, nutrons, true, 125", reader.readLine());
      assertEquals("1.5, 5, c, skskskks", reader.readLine());
      assertEquals(null, reader.readLine());

    } catch (IOException e) {
      fail("Something may have gone wrong with file reading. Or the code was bad  ¯\\_(ツ)_/¯ ");
    }
  }

  @Test
  public void testMulitpleFlushes() {
    initWriter(TEST_FILE_NAME, fields);

    //Step 1: do Step 1 from testLogging() (NOT step 2);

    assertEquals(4, testLogger.getCurrentFields().size());
    assertEquals(0, testLogger.getLinesToWrite().size());
    HashMap<String, Object> updateTest = new HashMap<>();

    updateTest.put("field1", 1);
    updateTest.put("field2", "yes");
    updateTest.put("field3", 'd');
    testLogger.update(updateTest);

    //Step 2: run testLogger.flushToFile()

    testLogger.flushToFile();

    try {

      //Step 3: do Step 3 from testCSVWriting(), but now there should only be one line written to
      // the file. So you just need to write 2 more assertEquals (1 for the first line of the
      // file, 1 that is testing for null)
      //
      //I wrote the first one again :)

      assertEquals("timestamp, field1, field2, field3", reader.readLine());
      assertEquals("0.5, 1, yes, d", reader.readLine());
      assertEquals(null, reader.readLine());


      updateTest.put("field2", true);
      updateTest.put("field1", "nutrons");
      updateTest.put("field3", 125);
      testLogger.update(updateTest);
      testLogger.flushToFile();

      assertEquals("1.0, nutrons, true, 125", reader.readLine());
      assertEquals(null, reader.readLine());

      updateTest.put("field3", "skskskks");
      updateTest.put("field1", 5);
      updateTest.put("field2", 'c');
      testLogger.update(updateTest);
      testLogger.flushToFile();

      assertEquals("1.5, 5, c, skskskks", reader.readLine());
      assertEquals(null, reader.readLine());

    } catch (IOException e) {
      fail("Something may have gone wrong with file reading. Or the code was bad  ¯\\_(ツ)_/¯ ");
    }
  }

  private class StepTimer implements LogTimer {
    double count;
    double step;

    public StepTimer(double step) {
      this.step = step;
      this.count = -step;
    }

    @Override
    public double getTimestamp() {
      count += step;
      return count;
    }
  }

}
