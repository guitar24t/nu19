package frc.team125.robot;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.commands.DrivePathCommand;

import frc.team125.robot.commands.PathFollowedByALine;
import frc.team125.robot.commands.groups.autocmd.SimpleAuto;
import frc.team125.robot.subsystems.Constants;
import frc.team125.robot.subsystems.Drivetrain;
import frc.team125.robot.subsystems.IntakeGoal;
import frc.team125.robot.subsystems.Superstructure;
import frc.team125.robot.subsystems.Vision;
import frc.team125.robot.utils.JoystickMap;

import java.util.ArrayList;
import java.util.Date;


/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  public static String autoPath;
  private final SendableChooser<String> chooser = new SendableChooser<>();
  private SendableChooser autoChooser;
  private ArrayList<String> profileModes;
  private Command autoCommand;
  private String autoSelected;

  public static OperatorInterface oi;
  public static Superstructure superstructure = new Superstructure();
  public static Drivetrain dt = new Drivetrain();
  public static PowerDistributionPanel pdp = new PowerDistributionPanel();
  public static Compressor compressor = new Compressor();
  public double suctionCurrent = pdp.getCurrent(12);

  public boolean globalHatchState = true;

  public static Vision vision = new Vision(
          RobotMap.TARGET_HEIGHT,
          RobotMap.CAM_HEIGHT,
          RobotMap.CAMERA_ANGLE);

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    this.compressor.start();
    this.compressor.clearAllPCMStickyFaults();

    this.vision.updateVisionDashboard();
    Date deployDate = new Date();
    autoChooser = new SendableChooser();
    // autoChooser.addDefault("Default Wait Command", )
    autoChooser.addObject("Rocket To Player Station Reverse", new DrivePathCommand("RocketToPlayerStationReverse"));
    autoChooser.addObject("Start To Rocket", new DrivePathCommand("StartToRocketShort"));
    autoChooser.addObject("Player Station To Rocket", new DrivePathCommand("PlayerStationToRocket"));
    autoChooser.addObject("Thirteen Foot Four", new DrivePathCommand("ThirtFtFour"));
    autoChooser.addObject("SarTocketAlingM", new PathFollowedByALine());


    SmartDashboard.putData("Auto Chooser 5000", autoChooser);

    // new Thread(() -> {
    //   UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
    //   camera.setResolution(1280, 720);
    //backVision.updateVisionDashboard();
    // new Thread(() -> {
    //  UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
    //UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
    //camera.setResolution(1280, 720);
    //   CvSink cvSink = CameraServer.getInstance().getVideo();
    //   CvSource outputStream = CameraServer.getInstance().putVideo("Reebot USB Camera", 1280, 720);

    //   Mat source = new Mat();
    //   Mat output = new Mat();

    //   while (!Thread.interrupted()) {
    //     cvSink.grabFrame(source);
    //     Imgproc.cvtColor(source, output, Imgproc.COLOR_BGR2GRAY);
    //     outputStream.putFrame(output);
    //   }
    // }).start();
    //  camera.setResolution(1280, 720);
    // Auto testing commands
    Command simpleDrive = new SimpleAuto();

    //this.wrist = new Wrist();
    this.vision.updateVisionDashboard();

    this.compressor.start();
    // This needs to be last since it initializes the starting teleop commands and everything else
    // needs to be initialized first
    this.oi = new OperatorInterface();
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test. This runs
   * after the mode specific periodic functions, but before LiveWindow and SmartDashboard integrated
   * updating.
   */
  @Override
  public void robotPeriodic() {
    Scheduler.getInstance().run();
    this.vision.updateVisionDashboard();

    SmartDashboard.putNumber("PDP Suction Cup Current", suctionCurrent);


  }

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro You can add
   * additional auto modes by adding additional comparisons to the switch structure below with
   * additional strings. If using the SendableChooser make sure to add them to the chooser code
   * above as well.
   */
  @Override
  public void autonomousInit() {
    this.dt.resetGyro();

    String autoPath = "PlingWow";

    // this.autoCommand = new DrivePathCommand(autoPath);
    // this.autoCommand = new AlignAndPlace();
    // this.autoCommand = new OppositeRocket1ToLVR();
    this.autoCommand = (Command) autoChooser.getSelected();


    this.dt.enableBrakeMode();
    this.autoCommand.start();
    this.vision.updateVisionDashboard();
    this.oi.launchPad.reconnect();
  }

  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }


  @Override
  public void disabledInit() {
    this.dt.updateSmartDashboard();
    this.dt.enableBrakeMode();
  }


  @Override
  public void disabledPeriodic() {
    this.dt.updateSmartDashboard();
    this.dt.enableBrakeMode();
    this.vision.updateVisionDashboard();
    this.superstructure.initialize();
    this.superstructure.updateSmartDashboard();

    if (this.superstructure.isArmForward()) {
      this.vision.setFrontCamFeed();
    } else {
      this.vision.setRearCamFeed();
    }

  }


  @Override
  public void teleopInit() {
    this.dt.enableCoastMode();
    this.dt.updateSmartDashboard();
    Scheduler.getInstance().run();
    this.vision.updateVisionDashboard();
    this.oi.launchPad.reconnect();
  }


  @Override
  public void teleopPeriodic() {
  //  SmartDashboard.putBoolean("Score Hatch", this.globalHatchState);
    SmartDashboard.putBoolean("Button pressed", this.oi.zero.isPressed());
    SmartDashboard.putBoolean("Button clicked", this.oi.zero.isClicked());

    Scheduler.getInstance().run();
    this.dt.updateSmartDashboard();
    this.superstructure.updateSmartDashboard();
    this.vision.updateVisionDashboard();
    this.dt.setAssisted(oi.driveAssisted.get());

    this.globalHatchState = this.superstructure.acquiringHatch;

    // All the buttons
    if (this.oi.zero.isPressed()) {
      this.superstructure.setGoal(Constants.ZERO);
    } else if (this.oi.topFront.isClicked()) {
      // Not sure if this is what we want here
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_HIGH_FRONT);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_HIGH_FRONT);
      }
    } else if (this.oi.midFront.isPressed()) {
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_MID_FRONT);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_MID_FRONT);
      }
    } else if (this.oi.horizontalFront.isPressed()) {
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_LOW_FRONT);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_LOW_FRONT);
      }
    } else if (this.oi.horizontalBack.isPressed()) {
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_LOW_BACK);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_LOW_BACK);
      }
    } else if (this.oi.topBack.isClicked()) {
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_HIGH_BACK);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_HIGH_BACK);
      }
    } else if (this.oi.midBack.isPressed()) {
      if (this.globalHatchState) {
        this.superstructure.setGoal(Constants.SCORE_HATCH_MID_BACK);
      } else {
        this.superstructure.setGoal(Constants.SCORE_CARGO_MID_BACK);
      }
    } else if (this.oi.intakeGndCargo.isClicked()) {
      this.superstructure.acquiringHatch = false;
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
      this.superstructure.setGoal(Constants.GROUND_CARGO_INTAKE);
    } else if (this.oi.intakePsCargo.isPressed()) {
      this.superstructure.acquiringHatch = false;
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
      this.superstructure.setGoal(Constants.HP_CARGO_INTAKE);
    } else if (this.oi.intakePsHatch.isPressed()) {
      this.superstructure.acquiringHatch = true;
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
      this.superstructure.setGoal(Constants.HP_HATCH_INTAKE);
    }

    // Wrist Intake Buttons
    if (this.oi.intakeUntil.isPressed()) {
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
    } else if (this.oi.intakeNormal.isPressed()) {
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN);
    } else if (this.oi.score.isPressed()) {
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.SCORING);
    } else if (this.oi.driverPad.getRawButtonReleased(JoystickMap.Y) || this.oi.driverPad.getRawButtonReleased(JoystickMap.RB)) {
      this.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.IDLE);
    }

    // Override game piece buttons
    if (this.oi.forceCargo.isPressed()) {
      this.superstructure.acquiringHatch = false;
    } else if (this.oi.forceHatch.isPressed()) {
      this.superstructure.acquiringHatch = true;
    }


    if (this.superstructure.justAcquiredGamePiece()) {
      this.vision.flash();
    } else {
      // Update which limelight is used
      if (this.superstructure.isArmForward()) {
        this.vision.setFrontCamFeed();
      } else {
        this.vision.setRearCamFeed();
      }
    }

    this.superstructure.update();
  }
}
