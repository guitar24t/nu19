/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.team125.robot.Robot;
import frc.team125.robot.subsystems.IntakeGoal;

public class ScoreHatchCmd extends Command {
  public ScoreHatchCmd() {
    super(0.2);
    requires(Robot.superstructure);
  }

  @Override
  protected void initialize() {
    Robot.superstructure.acquiringHatch = true;
    Robot.superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.SCORING);
  }

  @Override
  protected void execute() {
    Robot.superstructure.update();
  }

  @Override
  protected boolean isFinished() {
    return this.isTimedOut();
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
