/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.team125.robot.Robot;
import frc.team125.robot.subsystems.Superstructure;
import frc.team125.robot.subsystems.SuperstructureGoal;

public class MoveSuperstructureToPosition extends Command {
  SuperstructureGoal goal;

  public MoveSuperstructureToPosition(SuperstructureGoal goal) {
    this.goal = goal;
  }

  @Override
  protected void initialize() {
    Robot.superstructure.setGoal(goal);
  }

  @Override
  protected void execute() {
    Robot.superstructure.update();
  }

  @Override
  protected boolean isFinished() {
    return Robot.superstructure.isDone();
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}

