package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.Robot;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.Drivetrain;
import frc.team125.robot.utils.CsvWriter;
import jaci.pathfinder.Pathfinder;
import jaci.pathfinder.PathfinderFRC;
import jaci.pathfinder.followers.EncoderFollower;

import java.io.IOException;
import java.util.HashMap;

public class DrivePathCommand extends Command {
  String name;
  private EncoderFollower leftEncoderFollower;
  private EncoderFollower rightEncoderFollower;
  private Notifier notifier;
  private double targetDistanceToStop;
  private boolean reverse;
  private CsvWriter csvWriter;
  private double startHeading;

  public DrivePathCommand(String name) {
    this(name, name.contains("Reverse"), 0.0);
    requires(Robot.vision);
  }
  /**
   * Constructor for the DrivePathCommand class that never use.
   * 
   * @param name The name of the csv in use.
   * @param reverse If true then the path will go in reverse
   * @param targetDistanceToStop distance for the limelight to have to stop the path
   */

  public DrivePathCommand(String name, boolean reverse, double targetDistanceToStop) {
    requires(Robot.dt);
    this.targetDistanceToStop = targetDistanceToStop;
    this.reverse = reverse;
    this.name = name;
    String rioPath = PathfinderFRC.getTrajectoryFile(name).getAbsolutePath();
    System.out.println(String.format("Path to profile is %s", rioPath));
  }

  public DrivePathCommand withTargetDistance(double targetDistanceToStop) {
    this.targetDistanceToStop = targetDistanceToStop;
    return this;
  }
  
  @Override
  protected void initialize() {
    try{
    csvWriter = new CsvWriter("/home/lvuser/logs/drivePath" + this.name,
        new String[] {"velocity", "commandedVel", "heading", "commandedHead"});
    jaci.pathfinder.Trajectory leftTrajectory = PathfinderFRC.getTrajectory(name + ".right");
    jaci.pathfinder.Trajectory rightTrajectory = PathfinderFRC.getTrajectory(name + ".left");
    if (reverse) {
      jaci.pathfinder.Trajectory temp = leftTrajectory;
      rightTrajectory = temp;
      leftTrajectory = rightTrajectory;
      for (int i = 0; i < leftTrajectory.length(); i++) {
        leftTrajectory.get(i).velocity *= -1;
        System.out.println(leftTrajectory.get(i).velocity);
        rightTrajectory.get(i).velocity *= -1;
      }

    } else {
      leftTrajectory = PathfinderFRC.getTrajectory(name + ".right");
      rightTrajectory = PathfinderFRC.getTrajectory(name + ".left");
    }
 
    final double maxVelocity = Drivetrain.DrivetrainConstants.kMaxVelocity;

    // Step 2: Create EncoderFollow
    this.leftEncoderFollower = new EncoderFollower(leftTrajectory);
    this.rightEncoderFollower = new EncoderFollower(rightTrajectory);

    // Step 2.5: Configure EncoderFollowers
    this.leftEncoderFollower.configureEncoder(
        (int) Robot.dt.getLeftEncoderPosition(), 9, Drivetrain.DrivetrainConstants.kWheelDiameter);
    this.leftEncoderFollower.configurePIDVA(
        Drivetrain.DrivetrainConstants.assisted_kP, 0, Drivetrain.DrivetrainConstants.assisted_kD, 1.0 / maxVelocity, 0.0);

    this.rightEncoderFollower.configureEncoder(
        (int) Robot.dt.getLeftEncoderPosition(), 9, Drivetrain.DrivetrainConstants.kWheelDiameter);
    this.rightEncoderFollower.configurePIDVA(
        Drivetrain.DrivetrainConstants.assisted_kP, 0, Drivetrain.DrivetrainConstants.assisted_kD, 1.0 / maxVelocity, 0.0);
    Robot.dt.resetGyro();
    startHeading = leftTrajectory.get(0).heading;
    // Step 3: Start following
    notifier = new Notifier(this::followPath);
    notifier.startPeriodic(leftTrajectory.get(0).dt);

  } catch (IOException ioe) {
    System.out.println("Caught IOException");
    System.out.println(ioe.getStackTrace().getClass().getSimpleName());
    System.out.println(ioe.getMessage());
  }
  }
  
  private void followPath() {
    if (leftEncoderFollower.isFinished() || rightEncoderFollower.isFinished()) {
      csvWriter.write();
      notifier.stop();
    
    // else if (targetDistance is less than a certain distance){

    // }
    } else {
      double leftSpeed = this.leftEncoderFollower.calculate((int) Robot.dt.getLeftEncoderPosition());
      SmartDashboard.putNumber("Left Speed", leftSpeed);
      HashMap<String, Object> entries = new HashMap<>();
      entries.put("commandedVel", leftSpeed);
      entries.put("velocity", Robot.dt.getLeftVel());
      entries.put("heading", Robot.dt.getAngle());
      entries.put("commandedHead", Pathfinder.r2d(this.leftEncoderFollower.getHeading()));

      csvWriter.update(entries);
      csvWriter.write();

      int basePositionRight = (int) (reverse ? Robot.dt.getLeftEncoderPosition() : Robot.dt.getLeftEncoderPosition());
      double rightSpeed = this.rightEncoderFollower.calculate(basePositionRight);
      SmartDashboard.putNumber("Right Speed", rightSpeed);

      SmartDashboard.putNumber("Desired Position", this.rightEncoderFollower.getSegment().position);
      SmartDashboard.putNumber("Desired Velocity", this.rightEncoderFollower.getSegment().velocity);
      double mult = -1;//reverse ? -1 : 1;
      double heading = mult * Robot.dt.getAngle() - startHeading;

      SmartDashboard.putNumber("Heading", heading);

      double desiredHeading = Pathfinder.r2d(this.leftEncoderFollower.getHeading());
      SmartDashboard.putNumber("DesiredHeading", desiredHeading);

      double headingDifference = Pathfinder.boundHalfDegrees(desiredHeading - heading);
      SmartDashboard.putNumber("Heading Difference", headingDifference);
      //Reverse 1.25 worked
      double turnMultiplier = reverse ? 1.75 : 1.5;
      double turn = turnMultiplier * (-1.0 / 80.0) * headingDifference;
      SmartDashboard.putNumber("Turn", turn);
      Robot.dt.drive(leftSpeed + turn, rightSpeed - turn);
    }
  }
  

  
  

  @Override
  protected void execute() {
   // Robot.superstructure.update();
  }

  @Override
  protected boolean isFinished() {
    // if(wantTarget){
    //  return Robot.vision.targetVisible()
    double targetDistance = Robot.vision.calculateDistanceFromCameraHeight(
        RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE);
    boolean targetConditionDone = false;
    if (targetDistanceToStop > 0.0) {
      targetConditionDone = targetDistance < targetDistanceToStop;
    }
    return leftEncoderFollower.isFinished() || rightEncoderFollower.isFinished() || targetConditionDone;
  }

  @Override
  protected void end() {
    csvWriter.write();
    notifier.stop();
    if (targetDistanceToStop == 0) {
      Robot.dt.drive(0, 0);
    }
  }


}