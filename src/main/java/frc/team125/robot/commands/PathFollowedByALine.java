/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team125.robot.subsystems.Constants;
import frc.team125.robot.subsystems.SuperstructureGoal;

public class PathFollowedByALine extends CommandGroup {
  /**
   * Add your docs here.
   */
  public PathFollowedByALine() {
    addSequential(new DrivePathCommand("StartToRocketShort"));
    addSequential(new MoveSuperstructureToPosition(Constants.SCORE_HATCH_LOW_FRONT));
    addParallel(new AlignToTargetAndMoveForward(30));
    addSequential(new ScoreHatchCmd());
  }
}
