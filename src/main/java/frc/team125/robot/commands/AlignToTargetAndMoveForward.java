/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.Robot;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.Drivetrain;

/**
 * Command that Aligns the robot with the hatch port.
 *
 * @author rotciV Lumbago
 */
public class AlignToTargetAndMoveForward extends Command {
  private static double angleThresh = 0.7;
  private static double desiredTargetDistance = 50;
  private static double lastError = 0;

  /**
   * Will align to a target and drive forward.
   *
   * @param desiredDistance The desired distance to drive to.
   */
  public AlignToTargetAndMoveForward(double desiredDistance) {
    this.desiredTargetDistance = desiredDistance;
    requires(Robot.dt);
    requires(Robot.vision);
  }


  @Override
  protected void initialize() {
    Robot.dt.enableBrakeMode();
  }

  @Override
  protected void execute() {
    SmartDashboard.putNumber("Auto Turn", Drivetrain.DrivetrainConstants.auto_kP * Robot.vision.getXOffset());
    double distanceToTarget = Robot.vision.calculateDistanceFromCameraHeight(
            RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE);

    double error = distanceToTarget - this.desiredTargetDistance;

    double errorDiff = error - this.lastError;

    double distanceThrottle = error * Drivetrain.DrivetrainConstants.distance_kP
            - errorDiff * Drivetrain.DrivetrainConstants.distance_kD;

    double cameraDirection = Robot.vision.isUsingFrontCam() ? 1.0 : -1.0;


    Robot.dt.driveAssisted(distanceThrottle * cameraDirection, Robot.vision.getXOffset());
    lastError = error;
    SmartDashboard.putNumber("AlignMCommand Output", error
            * Drivetrain.DrivetrainConstants.distance_kP);

  }

  @Override
  protected boolean isFinished() {
    if (DriverStation.getInstance().isOperatorControl()) {
      return false;
    }
    return Math.abs(Robot.vision.getXOffset()) < angleThresh
            && Robot.vision.calculateDistanceFromCameraHeight(
            RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE) < desiredTargetDistance + 5
            && Robot.vision.calculateDistanceFromCameraHeight(
            RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE) > desiredTargetDistance - 5;
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
