package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.team125.robot.Robot;

public class ArcadeDriveCmd extends Command {
  double turnReduction = 0.4;

  public ArcadeDriveCmd() {
    requires(Robot.dt);
  }

  protected void initialize() {
    Robot.dt.enableCoastMode();
  }

  // Called repeatedly when this Command is scheduled to run
  protected void execute() {
    Robot.dt.driveArcade(Robot.oi.getDriverTriggerSum(), Robot.oi.getDriverLeftStickX() * turnReduction);
  }

  // Make this return true when this Command no longer needs to run execute()
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  protected void interrupted() {
  }
}

