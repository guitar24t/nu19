package frc.team125.robot.subsystems;

/**
 * Holds all the goals for the superstructure's individual subsystems.
 * Can then distribute the goals to the appropriate controllers.
 */
public class SuperstructureGoal {
  public double pivotGoal;
  public double armGoal;
  public double wristGoal;
  public boolean canBeReflected = false;
  public boolean intakingGoal = false;

  /** Constructor for the SuperstructureGoal.
   * @param p The pivot goal.
   * @param a The arm goal.
   * @param w The wrist goal.
   */
  public SuperstructureGoal(double p, double a, double w) {
    this.pivotGoal = Constants.Pivot.angleDegreesToEncoder(p);
    this.armGoal = a;
    this.wristGoal = Constants.Wrist.angleDegreesToEncoder(w);
  }

  /** Makes a SuperstructureGoal reflectable, smoothing out transitions between different goals.
   * @param canBeReflected The reflection state of a SuperstructureGoal.
   */
  public void setReflection(boolean canBeReflected) {
    this.canBeReflected = canBeReflected;
  }

  public void setIntakingGoal(boolean intake) {
    this.intakingGoal = intake;
  }
  /** Determines if the two SuperstructureGoals are the same goal.
   * @param other A different SuperstructureGoal to compare with.
   * @return If they are the same goal
   */
  public boolean sameObject(SuperstructureGoal other) {
    return this.pivotGoal == other.pivotGoal && this.armGoal == other.armGoal
            && this.wristGoal == other.wristGoal;
  }
}
