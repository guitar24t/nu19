package frc.team125.robot.subsystems;

public class Constants {

  /* The absolute positions for the superstructure
   Pivot down cargo side: 304
   Pivot down hatch side: 2642
   Arm all the way in: -765
   Arm all the way out: 27000
   Pivot straight up and down: 1790
   Arm at about 60 deg towards cargo: 971
   Arm at about 60 deg towards hatch: 1840

   Conversions:
   447 encoder clicks / cm
   434 = 90 - may be wrong
   2486 = -90 - may be wrong

   Wrist encoders:
   All the way back: 2900
   All the way forward: 4600
   "zero": 3980
   */

  // ZERO POSITIONS
  public static final double PIVOT_ZERO = 1824; //previous 1460
  public static final double ARM_SAFE_ZERO = 200;
  public static final double ARM_TRUE_ZERO = -700;
  public static final double WRIST_ZERO = 1768;
  public static final double CLICKS_PER_INCH = 1211.6; // (447 clicks / cm) * (2.54 cm / inch)
  public static final double CLICKS_PER_DEG = 11.4;

  // STATIC POSITIONS
  // Reflected states are set in the Superstructure
  // Same as the PREP_GROUND_CARGO_INTAKE
  public static final SuperstructureGoal CARRY = new SuperstructureGoal(20, ARM_SAFE_ZERO, 99);
  public static final SuperstructureGoal ZERO = new SuperstructureGoal(0, ARM_SAFE_ZERO, 0);

  public static final SuperstructureGoal HP_HATCH_INTAKE = new SuperstructureGoal(86, 0.0, 0.0);
  public static final SuperstructureGoal HP_CARGO_INTAKE = new SuperstructureGoal(45, 0.0, 0.0);

  public static final SuperstructureGoal SCORE_HATCH_LOW_FRONT = new SuperstructureGoal(90, ARM_SAFE_ZERO, 0);
  public static final SuperstructureGoal SCORE_HATCH_MID_FRONT = new SuperstructureGoal(35, 8493, 55);
  public static final SuperstructureGoal SCORE_HATCH_HIGH_FRONT = new SuperstructureGoal(23, 23283, 67);
  public static final SuperstructureGoal SCORE_HATCH_LOW_BACK = new SuperstructureGoal(-86, ARM_SAFE_ZERO, 0);
  public static final SuperstructureGoal SCORE_HATCH_MID_BACK = new SuperstructureGoal(-35, 8493, -55);
  public static final SuperstructureGoal SCORE_HATCH_HIGH_BACK = new SuperstructureGoal(-20, 23283, -70);

  public static final SuperstructureGoal SCORE_CARGO_LOW_FRONT = new SuperstructureGoal(64, ARM_SAFE_ZERO, 36);
  public static final SuperstructureGoal SCORE_CARGO_MID_FRONT = new SuperstructureGoal(14, 9682, 76);
  public static final SuperstructureGoal SCORE_CARGO_HIGH_FRONT = new SuperstructureGoal(8, 24260, 82);
  public static final SuperstructureGoal SCORE_CARGO_LOW_BACK = new SuperstructureGoal(-70, ARM_SAFE_ZERO, 0);
  public static final SuperstructureGoal SCORE_CARGO_MID_BACK = new SuperstructureGoal(-14, 9682, -76);
  public static final SuperstructureGoal SCORE_CARGO_HIGH_BACK = new SuperstructureGoal(-8, 24260, -82);

  public static final SuperstructureGoal GROUND_CARGO_INTAKE = new SuperstructureGoal(63, ARM_TRUE_ZERO, 99);
  public static final SuperstructureGoal GROUND_HATCH_INTAKE_FRONT = new SuperstructureGoal(0.0, 0.0, 0.0);
  public static final SuperstructureGoal GROUND_HATCH_INTAKE_BACK = new SuperstructureGoal(1800.0, 0.0, 0.0);


  public static class Pivot {
    // PIVOT CONSTANTS
    public static final double PIVOT_K_P = 10.0;
    public static final double PIVOT_K_I = 0.0;
    public static final double PIVOT_K_D = 300.0;
    public static final double PIVOT_K_F = 0.0;

    public static final double MAX_POWER = 1.0;

    /**
     * Returns the number of encoder clicks/native CTRE units for an angle given in degrees. Angles
     * are both positive and negative.
     *
     * @param angle The angle to be converted, given in degrees.
     * @return The equivalent number of encoder clicks/native CTRE units.
     */
    public static double angleDegreesToEncoder(double angle) {
      return Constants.PIVOT_ZERO - (angle * 11.4);
    }


    public static final int CRUISE_VEL = 1000;
    public static final int CRUISE_ACCEL = 600;
  }

  public static class Arm {
    // ARM CONSTANTS
    public static final double ARM_OUT_K_P = 2.0;
    public static final double ARM_OUT_K_I = 0.0;
    public static final double ARM_OUT_K_D = 0.0;
    public static final double ARM_OUT_K_F = 0.0;

    public static final double ARM_IN_K_P = 1.0;
    public static final double ARM_IN_K_I = 0.0;
    public static final double ARM_IN_K_D = 0.0;
    public static final double ARM_IN_K_F = 0.0;

    public static final double ARM_HOLDING_K_P = 0.5;
    public static final double ARM_HOLDING_K_I = 0.0;
    public static final double ARM_HOLDING_K_D = 0.0;
    public static final double ARM_HOLDING_K_F = 0.0;

    public static final double MAX_POWER = 1.0;

    // 220 cm / sec
    public static final int CRUISE_VELOCITY_UP = 18000;
    public static final int CRUISE_VELOCITY_DOWN = 18000;
    public static final int CRUISE_ACCEL_UP = 13000;
    public static final int CRUISE_ACCEL_DOWN = 8000;

  }

  public static class Wrist {
    // PIVOT CONSTANTS
    public static final double WRIST_K_P = 6.0;
    public static final double WRIST_K_I = 0.0;
    public static final double WRIST_K_D = 0.0;
    public static final double WRIST_K_F = 0.0;

    public static final double MAX_POWER = 1.0;

    public static final int CRUISE_VEL = 0;
    public static final int CRUISE_ACCEL = 0;

    /**
     * Returns the number of encoder clicks/native CTRE units for an angle given in degrees. Angles
     * are both positive and negative.
     *
     * @param angle The angle to be converted, given in degrees.
     * @return The equivalent number of encoder clicks/native CTRE units.
     */
    public static double angleDegreesToEncoder(double angle) {
      return Constants.WRIST_ZERO - (angle * 11.4);
    }
  }
}
