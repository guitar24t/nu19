package frc.team125.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team125.robot.RobotMap;


public class CargoIntake extends Subsystem {
  private CANSparkMax intakeMotor = new CANSparkMax(RobotMap.CARGO_INTAKE_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
  private Solenoid intakePiston = new Solenoid(RobotMap.CARGO_SOLENOID);

  private static final double INTAKE_POW = 0.6;

  /**
   * The intake that picks the cargo up from the ground.
   */
  public CargoIntake() {
    this.intakeMotor.setInverted(true);

    this.intakePiston.set(false);
  }

  public void runIntake() {
    intakeMotor.set(INTAKE_POW);
  }

  public void reverseIntake() {
    intakeMotor.set(-INTAKE_POW);
  }

  public void stopIntake() {
    intakeMotor.set(0);
  }

  public void extendPiston(boolean extend) {
    this.intakePiston.set(extend);
  }

  public boolean getPiston() {
    return this.intakePiston.get();
  }


  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
  }
}
