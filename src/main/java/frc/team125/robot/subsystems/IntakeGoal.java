package frc.team125.robot.subsystems;

public class IntakeGoal {
  public enum CargoIntakeGoal {
    NONE, RUN_FORWARD, RUN_REVERSE
  }

  public enum WristIntakeState {
    /*
    When the state is run, the intake will run. When the state is run_until, then it will run
    until the current spike has happened.
     */
    IDLE(0.0), REVERSE(-1.0), RUN(1.0), RUN_UNTIL(1.0), FLASHING(1.0),  CARRYING(0.01, 0.01), SCORING(0.5);

    private double cargo;
    private double hatch;

    WristIntakeState(double both) {
      this(both, both);
    }
    WristIntakeState(double cargo, double hatch) {
      this.cargo = cargo;
      this.hatch = hatch;
    }

    public double getPow(boolean hatch) {
      return hatch ? this.hatch : this.cargo;
    }
  }
}
