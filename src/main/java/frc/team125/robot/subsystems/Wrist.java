/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.IntakeGoal.WristIntakeState;
import frc.team125.robot.utils.Debouncer;

public class Wrist extends Subsystem {

  public enum ControlState {
    DISABLED, CALIBRATING, IDLE, MAGIC, MANUAL, HOLDING;
  }

  Timer lightTimer = new Timer();
  private TalonSRX wristMotor;
  private CANSparkMax intakeMotor;
  private Solenoid intakeToggle;

  private IntakeGoal.WristIntakeState currIntakeState;
  private ControlState currWristState;

  // Controls the state of the intake solenoid. True means intake closed for intake a hatch
  private boolean intakingHatch = true;
  private boolean hasGamePiece = false;

  private double tolerance = 15; // TODO tune this
  private double currWristPosGoal = 0.0;
  private double currWristOutput = 0.0;
  private double currIntakeOutput = 0.0;
  private double hatchDirection = -1.0; // Negative for hatches, positive for balls

  private Debouncer intakeCurrentDebouncer = new Debouncer(0.07);

  /**
   * Subsystem representing the wrist part of the superstructure.
   */
  public Wrist() {
    this.wristMotor = new TalonSRX(RobotMap.WRIST_MOTOR);
    this.intakeMotor = new CANSparkMax(RobotMap.INTAKE_MOTOR, MotorType.kBrushless);

    TalonSRXConfiguration wristConfig = new TalonSRXConfiguration();
    wristConfig.peakOutputForward = Constants.Wrist.MAX_POWER;
    wristConfig.peakOutputReverse = -Constants.Wrist.MAX_POWER;
    wristConfig.nominalOutputForward = 0.0;
    wristConfig.nominalOutputReverse = 0.0;

    this.wristMotor.configAllSettings(wristConfig);

    this.wristMotor.setInverted(true);
    this.wristMotor.setSensorPhase(true);
    this.wristMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
    this.wristMotor.getSensorCollection().setQuadraturePosition(
            this.wristMotor.getSensorCollection().getPulseWidthPosition(), 0);

    configMotionMagic(Constants.Wrist.CRUISE_VEL, Constants.Wrist.CRUISE_ACCEL);
    this.wristMotor.setNeutralMode(NeutralMode.Brake);

    // Configure the PID
    this.wristMotor.config_kP(0, Constants.Wrist.WRIST_K_P, 0);
    this.wristMotor.config_kI(0, Constants.Wrist.WRIST_K_I, 0);
    this.wristMotor.config_kD(0, Constants.Wrist.WRIST_K_D, 0);
    this.wristMotor.config_kF(0, Constants.Wrist.WRIST_K_F, 0);

    // INITIALIZE THE SOLENOIDS AND WRIST STATE
    this.intakeToggle = new Solenoid(RobotMap.INTAKE_TOGGLE_SOLENOID);

    this.currIntakeState = IntakeGoal.WristIntakeState.IDLE;
    this.currWristState = ControlState.IDLE;

  }


  /**
   * The main update function for the wrist where the state machine runs.
   *
   * @param hatch If we are we acquiring a hatch.
   */
  public void update(boolean hatch) {
    this.intakingHatch = hatch;
    this.hatchDirection = this.intakingHatch ? 1.0 : -1.0;
    this.currIntakeOutput = this.currIntakeState.getPow(this.intakingHatch) * this.hatchDirection;

    SmartDashboard.putString("intake State", this.currIntakeState.toString());
    // State of the intake, not necessarily the wrist motion
    switch (this.currIntakeState) {
      case RUN_UNTIL:
        this.hasGamePiece = this.intakeCurrentDebouncer.get();
        if (this.hasGamePiece) {
          this.lightTimer.reset();
          this.lightTimer.start();
          this.currIntakeState = WristIntakeState.FLASHING;
        }
        break;
      case FLASHING:
        if (this.lightTimer.get() > 0.5) {
          this.currIntakeState = WristIntakeState.CARRYING;
          this.lightTimer.stop();
        }
        break;
      case CARRYING:
        this.hasGamePiece = true;
        // Nothing else to do here, motor run with intake state
        break;
      case SCORING:
        this.hasGamePiece = false;
        this.currIntakeOutput *= -1.0;
        // Nothing else to do here, motor run with intake state
        break;
      default:
        break;
    }

    // This is the logic for moving the wrist.
    switch (this.currWristState) {
      case DISABLED:
        this.setStaticOutput(0.0);
        runWrist();
        this.currIntakeState = WristIntakeState.IDLE;
        break;
      case CALIBRATING:
        if (this.isCalibrated()) {
          this.currWristState = ControlState.IDLE;
        }
        break;
      case IDLE:
        this.setStaticOutput(0.0);
        runWrist();
        break;
      case MAGIC:
        runMotionMagic();
        if (this.checkTermination()) {
          this.currWristState = ControlState.HOLDING;
        }
        break;
      case MANUAL:
        //runWrist();
        break;
      case HOLDING:
        holdPosition();
        break;
      default:
        break;
    }


    // SEts the intake solenoid and motor power here
    this.intakeMotor.set(this.currIntakeOutput);
    this.intakeToggle.set(!this.intakingHatch);
    this.intakeCurrentDebouncer.update(this.intakeMotor.getOutputCurrent() > 22);
  }

  //////////////////////////////
  // ALL THE MOVEMENT METHODS //
  //////////////////////////////

  /**
   * Runs the wrist based on the output power set in setStaticOutput. Different from runWristManual.
   * This should be used in the state machine, the other should be used for testing only.
   */
  public void runWrist() {
    wristMotor.set(ControlMode.PercentOutput, this.currWristOutput);
  }

  /**
   * Runs wrist manually. Use for testing only. Use runWrist and setStaticOutput for state machine.
   *
   * @param pow wrist power
   */
  public void runWristManual(double pow) {
    this.currWristOutput = pow;
    this.wristMotor.set(ControlMode.PercentOutput, this.currWristOutput);
  }

  /**
   * Runs wrist to a goal using motion magic on the TalonSRXs.
   */
  public void runMotionMagic() {
    //this.wristMotor.set(ControlMode.MotionMagic, this.currWristPosGoal);
    this.wristMotor.set(ControlMode.Position, this.currWristPosGoal);
  }

  /**
   * Holds the pivot at a specific position using the Position Closed Loop Control.
   */
  public void holdPosition() {
    // Check to see if we're actually at position
    // If we're not at position, don't try to move, just hold the position
    // TODO fix this logic later
    this.wristMotor.set(ControlMode.Position, this.currWristPosGoal);
  }

  /////////////////////////////////
  // ALL THE SETTERS AND CONFIGS //
  /////////////////////////////////

  /**
   * Used to the set the goal for the motion magic OR position control (HOLDING).
   *
   * @param g The goal, given as a double representing encoder clicks/native CTRE units
   */
  public void setMagicPosGoal(double g) {
    this.currWristState = ControlState.MAGIC;
    this.currWristPosGoal = g;
  }

  public void setIntakeState(IntakeGoal.WristIntakeState s) {
    this.currIntakeState = s;
  }

  /**
   * Sets the state of the wrist.
   *
   * @param s Desired wrist state.
   */
  public void setState(ControlState s) {
    this.currWristState = s;
  }

  /**
   * This is used for setting a static output for the wrist. Will run the wrist at that power in the
   * runWrist method (the MANUAL/IDLE state). This method should be called by the superstructure.
   *
   * @param pow Takes a power values, sets that to the current output.
   */
  public void setStaticOutput(double pow) {
    this.currWristOutput = pow;
  }

  /**
   * Configures the velocity and acceleration for motion magic.
   *
   * @param cruiseVelocity     The max velocity of the system (cruise velocity)
   * @param cruiseAcceleration The max acceleration of the system (cruise accel)
   */
  public void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
    wristMotor.configMotionCruiseVelocity(cruiseVelocity);
    wristMotor.configMotionAcceleration(cruiseAcceleration);
  }

  /////////////////////
  // ALL THE GETTERS //
  /////////////////////

  /**
   * Use this for determining if we currently have a hatch or a cargo.
   *
   * @return The state of the clamp, which is closed when we have a cargo, open for hatch.
   */
  public boolean inHatchPosition() {
    return this.intakingHatch;
  }

  /**
   * Returns true if we currently have a game piece, hatch or cargo
   *
   * @return True if game piece in intake
   */
  public boolean hasGamePiece() {
    return this.hasGamePiece;
  }

  /**
   * Method that calculates the angle of the wrist given the current encoder position.
   *
   * @return The current angle in degrees.
   */
  public double getAngleDegrees() {
    return (Constants.WRIST_ZERO - this.getEncoderClicks()) / Constants.CLICKS_PER_DEG;
  }

  /**
   * Returns the number of encoder clicks for an angle given in degrees. Angle ranges - to +
   *
   * @param angle The angle to be ocnverted, given in degrees.
   * @return The equivalent number of encoder clicks.
   */
  public double angleToClicks(double angle) {
    return Constants.WRIST_ZERO - (angle * Constants.CLICKS_PER_DEG);
  }

  /**
   * Simply returns the current number of encoder clicks.
   *
   * @return The current number of encoder clicks.
   */
  public double getEncoderClicks() {
    return this.wristMotor.getSelectedSensorPosition();
  }


  /**
   * Calculates whether or not the wrist is at its position and ready to move to the next part of
   * the state machine.
   *
   * @return If the wrist is done moving or not.
   */
  public boolean checkTermination() {
    return Math.abs(this.getEncoderClicks() - this.currWristPosGoal) <= this.tolerance;
  }

  /**
   * Considered done if the wrist is in the HOLDING state.
   *
   * @return If the arm is done with it's predetermined set of movements.
   */
  public boolean isDone() {
    return this.currWristState == ControlState.HOLDING;
  }

  /**
   * Determines if the wrist has been calibrated. Will only start the state machine if it's in the
   * proper starting position.
   *
   * @return Whether the robot is in the correct spot/calibrated.
   */
  public boolean isCalibrated() {
    return (this.getEncoderClicks() < 0.0 && this.getEncoderClicks() > 0.0);
  }

  /**
   * Used to find the current of the suction cup motor. This can be used to knowing when a game
   * piece has been acquired.
   *
   * @return The current of the suction motor.
   */
  public double getIntakeCurrent() {
    return this.intakeMotor.getOutputCurrent();
  }

  public WristIntakeState getIntakeState() {
    return this.currIntakeState;
  }

  /**
   * Prints necessary information to the smartdashboard.
   */
  public void updateSmartDashboard() {
    SmartDashboard.putNumber("Wrist Selected Sensor Position", this.getEncoderClicks());
    SmartDashboard.putNumber("Wrist Feedback Error:",
            Math.abs(this.getEncoderClicks() - this.currWristPosGoal));
    SmartDashboard.putNumber("Wrist Output:", this.currWristOutput);
    SmartDashboard.putString("Wrist State:", this.currWristState.toString());
    SmartDashboard.putNumber("Wrist Angle:", this.getAngleDegrees());
    SmartDashboard.putNumber("Wrist Goal:", this.currWristPosGoal);
    SmartDashboard.putBoolean("Intaking hatch", this.intakingHatch);
    SmartDashboard.putBoolean("Intake solenoid", this.intakeToggle.get());
    SmartDashboard.putNumber("Intake current", this.getIntakeCurrent());
    SmartDashboard.putBoolean("Has game piece", this.hasGamePiece());
    SmartDashboard.putNumber("Intake output", this.currIntakeOutput);
  }

  @Override
  public void initDefaultCommand() {
  }

}
