package frc.team125.robot;

public class RobotMap {

  public static final int WRIST_MOTOR = 10;
  public static final int PIVOT_MOTOR = 14;
  public static final int ARM_MOTOR = 12;
  public static final int CARGO_INTAKE_MOTOR = 4;

  //Drivetrain
  public static final int LEFT_DRIVE_MAIN = 1;
  public static final int RIGHT_DRIVE_MAIN = 6;

  public static final int LEFT_DRIVE_FOLLOWER = 2;
  public static final int RIGHT_DRIVE_FOLLOWER = 5;

  //Suction Cup
  public static final int INTAKE_MOTOR = 3;

  // Solenoid

  public static final String LIMELIGHT_BACK = "limelight-fcam";
  public static final String LIMELIGHT_FRONT = "limelight-bcam";
  public static final int INTAKE_TOGGLE_SOLENOID = 1;
  public static final int CARGO_SOLENOID = 0;

  // Vision
  public static final double TARGET_HEIGHT = 29;
  public static final double CAM_HEIGHT = 23.75;
  public static final double CAMERA_ANGLE = Math.toRadians(10.1);
  public static final double BACK_CAMERA_ANGLE = Math.toRadians(0.0);
  public static final double ROCKET_PORT_HEIGHT = 39.125;

}
