package frc.team125.robot;

import edu.wpi.first.wpilibj.Joystick;

import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.team125.robot.commands.AlignToTargetAndMoveForward;
import frc.team125.robot.commands.AssistedDriveCmd;
import frc.team125.robot.utils.*;

import java.net.URI;
import java.net.URISyntaxException;

public class OperatorInterface {
  private static final double STICK_DEADBAND = 0.01;
  public Joystick driverPad = new Joystick(0);
  public Joystick opPad = new Joystick(1);
  public JoystickButton driveAssisted = new JoystickButton(driverPad, JoystickMap.B);
  public JoystickButton alignMove = new JoystickButton(driverPad, JoystickMap.X);

  // Driver normal buttons
  public NutButton intakeUntil = new NutButton(new JoystickButton(this.driverPad, JoystickMap.LB));
  public NutButton score = new NutButton(new JoystickButton(this.driverPad, JoystickMap.RB));
  public NutButton intakeNormal = new NutButton(new JoystickButton(this.driverPad, JoystickMap.Y));
  public NutButton forceCargo = new NutButton(new JoystickButton(this.driverPad, JoystickMap.START));
  public NutButton forceHatch = new NutButton(new JoystickButton(this.driverPad, JoystickMap.BACK));


  // Operator NutButtons
  public NutButton zero;
  public NutButton horizontalFront;
  public NutButton horizontalBack;
  public NutButton midFront;
  public NutButton midBack;
  public NutButton topFront;
  public NutButton topBack;
  public NutButton intakePsHatch;
  public NutButton intakePsCargo;
  public NutButton intakeGndCargo; // TODO brian

  public LaunchPad launchPad;

  {
    try {
      launchPad = new LaunchPad(new URI("ws://10.1.25.69:9001"));
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  /**
   * Contains all the buttons and initializes commands for the driver gamepads.
   */
  public OperatorInterface() {
    this.alignMove.whileHeld(new AlignToTargetAndMoveForward(30.0));
    this.driveAssisted.whileHeld(new AssistedDriveCmd());

    launchPad.connect();

    horizontalFront = new NutButton(new MultiButton(this.launchPad.getButton(0, 8), this.launchPad.getButton(1, 8),
            this.launchPad.getButton(0, 7), this.launchPad.getButton(1,7)));
    horizontalBack = new NutButton(new MultiButton(this.launchPad.getButton(6,7), this.launchPad.getButton(7,7),
            this.launchPad.getButton(6,8),this.launchPad.getButton(7,8)));
    midFront = new NutButton(new MultiButton(this.launchPad.getButton(0,4), this.launchPad.getButton(0,5),
            this.launchPad.getButton(1,4), this.launchPad.getButton(1,5)));
    midBack = new NutButton(new MultiButton(this.launchPad.getButton(6,4), this.launchPad.getButton(7,4),
            this.launchPad.getButton(6,5),this.launchPad.getButton(7,5)));
    topFront = new NutButton(new MultiButton(this.launchPad.getButton(0 , 1), this.launchPad.getButton(1,1),
            this.launchPad.getButton(0, 2), this.launchPad.getButton(1, 2)));
    topBack = new NutButton(new MultiButton(this.launchPad.getButton(6 , 1), this.launchPad.getButton(7,1),
            this.launchPad.getButton(6, 2), this.launchPad.getButton(7, 2)));

    intakePsHatch = new NutButton(new MultiButton(this.launchPad.getButton(3, 4), this.launchPad.getButton(4,4)));
    intakePsCargo = new NutButton(new MultiButton(this.launchPad.getButton(3, 6), this.launchPad.getButton(4,6)));
    intakeGndCargo = new NutButton(new MultiButton(this.launchPad.getButton(3, 8), this.launchPad.getButton(4,8)));

    zero = new NutButton(new MultiButton(this.launchPad.getButton(3, 1), this.launchPad.getButton(4,1),
            this.launchPad.getButton(3, 2), this.launchPad.getButton(4, 2)));
  }

  private static double stickDeadband(double value, double deadband, double center) {
    return (value < (center + deadband) && value > (center - deadband)) ? center : value;
  }

  public double getDriverLeftStickY() {
    return stickDeadband(this.driverPad.getRawAxis(JoystickMap.LEFT_Y), STICK_DEADBAND, 0.0);
  }

  public double getDriverLeftStickX() {
    return stickDeadband(this.driverPad.getRawAxis(JoystickMap.LEFT_X), STICK_DEADBAND, 0.0);
  }

  public double getDriverRightStickY() {
    return stickDeadband(this.driverPad.getRawAxis(JoystickMap.RIGHT_Y), STICK_DEADBAND, 0.0);
  }

  public double getDriverRightStickX() {
    return stickDeadband(this.driverPad.getRawAxis(JoystickMap.RIGHT_X), STICK_DEADBAND, 0.0);
  }

  public double getDriverTriggerSum() {
    return this.driverPad.getRawAxis(JoystickMap.RIGHT_TRIGGER)
            - this.driverPad.getRawAxis(JoystickMap.LEFT_TRIGGER);
  }

  public double getOpRightTrigger() {
    return this.opPad.getRawAxis(JoystickMap.RIGHT_TRIGGER);
  }

  public double getOpLeftStickX() {
    return stickDeadband(this.opPad.getRawAxis(JoystickMap.LEFT_X), STICK_DEADBAND, 0.0);
  }

  public double getOpRightStickX() {
    return stickDeadband(this.opPad.getRawAxis(JoystickMap.RIGHT_X), STICK_DEADBAND, 0.0);
  }

  public double getOpLeftStickY() {
    return stickDeadband(this.opPad.getRawAxis(JoystickMap.LEFT_Y), STICK_DEADBAND, 0.0);
  }

  public double getOpRightStickY() {
    return stickDeadband(this.opPad.getRawAxis(JoystickMap.RIGHT_Y), STICK_DEADBAND, 0.0);
  }

}


