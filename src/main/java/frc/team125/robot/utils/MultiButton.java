package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.buttons.Button;

import java.util.ArrayList;
import java.util.Arrays;

public class MultiButton extends Button {

  private ArrayList<Button> buttons;

  public MultiButton(Button... buttons) {
    this.buttons = new ArrayList<Button>(Arrays.asList(buttons));
  }

  @Override
  public boolean get() {
    for (Button button :
            buttons) {
      if (button.get()) {
        return true;
      }
    }

    return false;
  }
}
