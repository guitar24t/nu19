package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.Timer;

class WpiTimer implements LogTimer {

  @Override
  public double getTimestamp() {
    return Timer.getFPGATimestamp();
  }
}