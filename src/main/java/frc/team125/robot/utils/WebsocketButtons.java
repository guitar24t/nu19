package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

public class WebsocketButtons extends JoystickButton {

  private static final GenericHID m_joystick = null;
  private boolean buttonState = false;

  WebsocketButtons() {
    super(m_joystick, 0);
  }

  public void set(boolean newValue) {
    buttonState = newValue;
  }

  public boolean get() {
    return buttonState;
  }
}

