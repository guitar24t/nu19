package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.buttons.Button;

public class NutButton {

  private Boolean lastButtState = false;
  private Button nuttuButtu;

  public NutButton(Button nuttuButtu) {
    this.nuttuButtu = nuttuButtu;
  }



  public boolean isClicked() {
    boolean retVal = false;
    if (nuttuButtu.get() && !lastButtState) {
      retVal = true;
    }
    this.lastButtState = this.nuttuButtu.get();
    return retVal;
  }

  public boolean isPressed() {
    return nuttuButtu.get();
  }

  public boolean isReleased() {
    boolean retVal = false;
    if (!nuttuButtu.get() && lastButtState) {
      retVal = true;
    }
    this.lastButtState = this.nuttuButtu.get();
    return retVal;
  }
}
