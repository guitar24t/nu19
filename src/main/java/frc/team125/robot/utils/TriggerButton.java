package frc.team125.robot.utils;


import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;

public class TriggerButton extends Button {
    private Joystick joystick;
    private int triggerAxis;
    private double threshold = .75;

    public TriggerButton(Joystick joystick, int triggerAxis, double threshold){
        this.joystick = joystick;
        this.triggerAxis = triggerAxis;
        this.threshold = threshold;
    }

    public TriggerButton(Joystick joystick, int triggerAxis){
        this.joystick = joystick;
        this.triggerAxis = triggerAxis;
    }

    @Override
    public boolean get() {
        return joystick.getRawAxis(triggerAxis) > threshold;
    }

}

