package frc.team125.robot.utils;

import java.net.URI;
import java.util.ArrayList;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class LaunchPad extends WebSocketClient {
  int x;
  int y;
  int state;
  private WebsocketButtons[][] buttons = new WebsocketButtons[9][9];

  public LaunchPad(URI uri) {
    super(uri);

    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        this.buttons[i][j] = new WebsocketButtons();
      }
    }
  }

  public WebsocketButtons getButton(int x, int y) {
    return this.buttons[x][y];
  }

  @Override
  public void onMessage(String message) {
    if (!message.contains(":")) {
      x = -1;
      y = -1;
      state = -1;
      return;
    }

    ArrayList<Character> fullMessage = new ArrayList<Character>();
    //Creates a list of the characters in the message
    for (int i = 0; i < message.length(); i++) {
      fullMessage.add(message.charAt(i));
    }
    //x:y:state
    //Gets the x value of the message
    x = Integer.parseInt(fullMessage.get(0).toString());
    //Gets y
    y = Integer.parseInt(fullMessage.get(2).toString());
    //Gets state
    String stateStr = message.substring(4);
    state = Integer.parseInt(stateStr);

    if (state == 127) {
      this.send(String.format("pressed:%d:%d", x, y));
      buttons[x][y].set(true);
    }
    if (state == 0) {
      this.send(String.format("released:%d:%d", x, y));
      buttons[x][y].set(false);
    }

  }

  @Override
  public void onError(Exception e) {
    System.out.println(e.getMessage());
  }

  @Override
  public void onClose(int code, String reason, boolean remote) {
    System.out.println("Closing button server");
  }

  @Override
  public void onOpen(ServerHandshake hs) {

  }

}

